#!/bin/sh
TODAY=$(date -Isecond)

echo "$TODAY Waiting for MySQL Server to spin up..."
sleep 25
echo "$TODAY Done"

cd /app && sh prep.sh
